

<br>
<div class="jumbotron">
  <div class="container">
    <h1>Gestor de Imagenes con PHP</h1>
    <p>Esto es un gestor de imagenes, programado con PHP, donde podras autenticarte como usuario administrador, y subir imagenes, asi como crear diferentes directorios en la web. Estamos usando  Bootstrap para los estilos, asi como el Plugin llamado Lightbox2, para poder mostrar las imagenes desde la web.</p>
    <p>Puedes descargarte el codigo integro, desde <a href="https://bitbucket.org/davidfraj/directoriosphp">https://bitbucket.org/davidfraj/directoriosphp</a></p>
    
  </div>
</div>

